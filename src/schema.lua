local typedefs = require "kong.db.schema.typedefs"

return {
    name = "spada-auth",
    fields = {
        { 
            endpoint = typedefs.url{ 
                required = true 
            }
        },
        { 
            bearer = { 
                type = "string" ,
                required = true,
                default = "Authorization"
            }
        }
    }
}