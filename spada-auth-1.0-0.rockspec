package = "spada-auth"
version = "1.0.0"

source = {
        url = "git://gitlab.com/willymuhammad/kong-auth"
}

description = {
  summary = "A Kong plugin, that let you use an external Oauth 2.0 provider to protect your API",
  license = "Apache 2.0"
}

dependencies = {
  "lua >= 5.1"
  -- If you depend on other rocks, add them here
}

build = {
  type = "builtin",
  modules = {
    ["kong.plugins.spada-auth.handler"] = "src/handler.lua",
    ["kong.plugins.spada-auth-introspection.schema"] = "src/schema.lua",
    ["kong.plugins.spada-auth.access"] = "src/access.lua",
  }
}